//导入常用io traits
use std::io::prelude::*;
//导入TcpStream
use std::net::TcpStream;
//导入TcpListener
use std::net::TcpListener;
//导入外部代码 chrono
extern crate chrono;
//导入错误处理
use std::io::Read;
//导入需要的struct和trait
use chrono::{DateTime, FixedOffset, Local, Utc};

//主函数，程序入口
fn main() {
    //返回一个TcpListener实例，绑定到本机8080端口
    let listener_wrap = TcpListener::bind("127.0.0.1:80");

    //Result模式匹配，OK处理unwrap，Err输出错误并panic。
    let listener=match listener_wrap {
        Ok(listener_unwrap) =>{listener_unwrap},
        Err(e) => {
            println!("错误信息：{:?}",e);
            panic!("fail");
        }
    };

    //接受连接
    for stream_wrap in listener.incoming() {
        //模式匹配对Result进行处理
        let  stream = match stream_wrap {
            Ok(stream_unwrap) => {stream_unwrap},
            Err(e) => {
                println!("错误信息：{:?}",e);
                panic!("fail");
            }
        };

        //处理连接
        handle_connection(stream);
    }
}

//处理连接的函数
fn handle_connection(mut stream: TcpStream) {
    //出于方便，在栈中建缓冲区
    let mut buffer = [0; 1024];
    //读取数据到缓冲区，带模式匹配处理异常抛出
    match stream.read(&mut buffer) {
        Ok(_) => (),
        Err(io_error) => {
            println!("错误信息：{:?}",io_error);
            panic!("fail");
        }
    };

    //字节转换为字符串，过滤不可识别部分
    let client_content = String::from_utf8_lossy(&buffer);
    //服务端日志：显示时间和事件
    println!("{}  Client Request: {}", china_time(),client_content);
    //响应内容准备
    let server_response ="EchoServer Response:".to_string()+ &client_content;

    //字符串转为字节，写入连接
    match stream.write(server_response.as_bytes()) {
        Ok(_) => (),
        Err(io_error) => {
            println!("错误信息：{:?}",io_error);
            panic!("fail");
        }
    };

    //阻塞程序直到所有字节写入连接中
    match stream.flush() {
        Ok(_) => (),
        Err(io_error) => {
            println!("错误信息：{:?}",io_error);
            panic!("fail");
        }
    };

    //服务端日志：显示时间和事件
    println!("{}  {}", china_time(),server_response);
}

//格式化输出本地时间脚手架函数
fn china_time() -> String {
    //生成本地时间DateTime
    let local_time = Local::now();
    //生成UTC时间，DateTime实例
    let utc_time = DateTime::<Utc>::from_utc(local_time.naive_utc(), Utc);
    //时区偏移
    let china_timezone = FixedOffset::east(8 * 3600);
    //设置时区生成DateTime
    let dt=utc_time.with_timezone(&china_timezone);
    //格式化日期时间
    dt.format("%Y-%m-%d %H:%M:%S").to_string()
}